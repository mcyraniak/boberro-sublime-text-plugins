import sublime, sublime_plugin

class BoberroCopyFilenameCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        file_name = (self.view.file_name() or "").split("/")[-1]
        sublime.set_clipboard(file_name);

import sublime, sublime_plugin, os

class BoberroTemplateCommand(sublime_plugin.TextCommand):
	def run(self, edit):
		self.edit = edit
		self.new_view = self.view.window().new_file()
		self.settings = sublime.load_settings("boberro_plugin.sublime-settings")
		self.options = []
		for root, dirs, files in os.walk( self.settings.get("templates_path") ):
			for name in files:
				self.options.append(os.path.join(root, name))
		self.new_view.window().show_quick_panel(self.options, self.on_selected, sublime.MONOSPACE_FONT)

	def on_selected(self, index):
		with open(self.options[index], 'r') as f:
			self.new_view.insert(self.edit, 0, f.read())